# TCP_Testing_and_Alignment

## Validating the alignment of ns-3 CUBIC against Linux CUBIC using ns-3 Direct Code Execution (DCE)

Download ns-3 CUBIC code from following repository:
https://gitlab.com/tomhenderson/ns-3-dev/tree/tcp-cubic

Steps to reproduce results:

1) Install DCE in your system.

2) Copy tcp-cubic.{cc,h} file into source/ns-3-dev/src/internet/model/ directory and update the wscript. 

3) Build DCE again to use ns-3 TCP CUBIC in ns-3-dce.

4) Copy dumbbell-topology-cubic-variant.cc file in ns-3-dce/examples folder and update the wscript.

5) Copy parse_cwnd.py file from Scripts folder in source/ns-3-dce/utils folder.

Instructions for runnung the example:

1) Run following command in source/ns-3-dce directory:

   For Linux stack:

   i) ./waf --run "dumbbell-topology-cubic-variant --stack=linux --queue_disc_type=FifoQueueDisc --WindowScaling=true --Sack=true --stopTime=300 --delAckCount=2 --BQL=true"
   ii) cd utils/
   iii) python parse_cwnd.py 2 2

   For ns-3 stack:

   ./waf --run "dumbbell-topology-cubic-variant --stack=ns3 --queue_disc_type=FifoQueueDisc --WindowScaling=true --Sack=true --delAckCount=2 --dataSize=524 --stopTime=300 --recovery=TcpLinuxPrrRecovery --BQL=true"

2) After running the above commands, a 'result' folder will be created in the source/ns-3-dce directory. This folder contains a folder named dumbbell-topology. This folder contains all the results named according to the start date and time of the simulation. Also, there will be cwnd_data folder which will contain the cwnd traces for Linux stack.

3) Go to the required folder. In this folder, various folders will be created containing different kinds of statistics like cwnd traces for ns-3 stack, queue length at the router, packets dropped at the router and pcap files for all the interfaces

The results obtained can be plotted using custom scripts. Following link contains the result for validation of TCP CUBIC: 
https://gitlab.com/apoorvabhargava/tcp_testing_and_alignment/wikis/TCP-CUBIC-Validation-Results
